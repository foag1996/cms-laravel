<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');//relacion con la tabla usuario
            $table->unsignedBigInteger('category_id');//relacion tabla categorias
            $table->string('name');
            $table->string('slug')->unique();
            $table->mediumText('abstract')->unique();//campo para el resumen
            $table->text('body')->unique();//cuerpo
            $table->enum('status', ['PUBLISHED', 'DRAFT'])->default('DRAFT');//campo para poder cambiar el estado
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');//llave foranea y referencia de la tabal de donde trae el usuario
            $table->foreign('category_id')->references('id')->on('categories');//llave foranea y referencia de la tabal de donde trae el id

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
