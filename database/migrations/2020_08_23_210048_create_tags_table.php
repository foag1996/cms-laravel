<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');//relacion para tener el id de categorias y colocarlo en tags
            $table->string('name');  
            $table->string('slug'); //mismo nombre de la categoria sin mayusculas ni caracteres url de la pagina
            $table->softDeletes(); //guarda los registros y desaoarecen de nuestra vista
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories');//llave foranea y referencia de la tabal de donde trae el id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tags');
    }
}
