<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subcategories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');//relacion para tener el id de categorias y colocarlo en subcategotias
            $table->string('name');  
            $table->string('slug'); 
            $table->softDeletes(); 
            $table->timestamps();
            $table->foreign('category_id')->references('id')->on('categories');//llave foranea y referencia de la tabal de donde trae el id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subcategories');
    }
}
