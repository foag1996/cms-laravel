<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use Softdeletes; //muestra la fecha de cuando es eliminado
    protected $fillable = [
        'user_id', 'category_id', 'name', 'slug', 'abstract', 'body', 'status',
    ];
    protected $dates=['deleted_at'];
    protected $hidden=['created_at', 'updated_at'];

    //relacion 1:1 de image y post
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
}
