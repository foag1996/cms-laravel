<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use Softdeletes; //muestra la fecha de cuando es eliminado
    protected $fillable = [
        'name', 'module', 'slug',
    ];
    protected $dates=['deleted_at'];
    protected $hidden=['created_at', 'updated_at'];
    //relacion de categoria y subcategorias
    public function subcategories(){ //nombre en plural para no tener errores
         return $this->hasMany(Subcategory::class);//relacion 1 a muchos
    }
    //relacion de categoria y tag
    public function tags(){ //nombre en plural para no tener errores
        return $this->hasMany(Tag::class);//relacion 1 a muchos
   }
}
