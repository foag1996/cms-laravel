<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcategory extends Model
{
    use Softdeletes; //muestra la fecha de cuando es eliminado
    protected $fillable = [
        'name', 'category_id', 'slug',
    ];
    protected $dates=['deleted_at'];
    protected $hidden=['created_at', 'updated_at'];

    //relacion de categoria y subcategorias
    public function category(){ //nombre en singular para no tener errores
        return $this->belongsTo(Category::class);//relacion 1 a 1
   }
}
