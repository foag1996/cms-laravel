<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
    use Softdeletes; //muestra la fecha de cuando es eliminado
    protected $fillable = [
        'name', 'category_id', 'slug',
    ];
    protected $dates=['deleted_at'];
    protected $hidden=['created_at', 'updated_at'];
    //relacion de tag y productos 
    public function products(){ //nombre en plural para no tener errores
         return $this->belongsToMany(Product::class);//relacion de 1 a n
    }
    //relacion de tag y categorias
    public function categories(){ //nombre en plural para no tener errores
        return $this->belongsToMany(Category::class); //relacion de 1 a n
   }
}
