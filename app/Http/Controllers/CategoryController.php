<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
   
    public function index() //lista todas las categorias registradas
    {
        $categories= Category::orderBy('id', 'DESC')->paginate(15);
        return view('admin.categories.index', compact('categories'));
    }

   
    public function create()
    {
        return view('admin.categories.create');
    }

    
    public function store(Request $request)
    {
        //validacion
        $request->validate([
            'name'=> 'required|unique:categories|max:20',
            'module'=> 'required|max:20',
        ]);

        //slug automatico, la (e) es para que guarde en la BD
        $category=new Category;
        $category->name=e($request->name);
        $category->module=e($request->module);
        $category->slug=Str::slug($request->name);

        //guardar informacion
        $category->save();
        return redirect()->route('categories.index')->with('info', 'Categoría agregada');
    }

    //function module para listar el modulo
    public function module($module)
    {
        $categories=Category::where('module', $module)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.categories.index', compact('categories'));


    }

   
    public function show(Category $category)
    {
        //
    }

    
    public function edit($id)
    {
        //editar una categoria
        $category=Category::where('id', $id)->firstOrFail();
        return view('admin.categories.edit', compact('category'));
    }

    
    public function update(Request $request, $id)
    {
        //se validan los datos
        $request->validate([
            'name'=> 'required|max:20',
            'module'=> 'required|max:20',
        ]);

        //se actualiza con el $id
        $category=Category::findOrFail($id);
        $category->name=e($request->name);
        $category->module=e($request->module);
        $category->slug=Str::slug($request->name);

         //se guarda y retorna a la vista
        $category->save();
        return redirect()->route('categories.index')->with('info', 'Actualizado correctamente');

    }

    
    public function destroy($id)
    {
        //alimina la categoria
        $category=Category::findOrFail($id)->delete();
        //retorna a la vista anterior
        return back()->with('info', 'Eliminado con éxito');

    }
}
