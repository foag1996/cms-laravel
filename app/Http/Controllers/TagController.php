<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class TagController extends Controller
{
    public function index() //lista todas las categorias registradas
    {
        $tags= Tag::orderBy('id', 'DESC')->paginate(15);
        return view('admin.tags.index', compact('tags'));
    }

   
    public function create()
    {
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');//a q categoria pertence el tag
        return view('admin.tags.create', compact('categories'));
    }

    
    public function store(Request $request)
    {
        //validacion
        $request->validate([
            'name'=> 'required|unique:tags|max:20',

        ]);

        //slug automatico, la (e) es para que guarde en la BD
        $tag=new Tag;
        $tag->name=e($request->name);
        $tag->category_id=e($request->category_id); //la (e) es para que guarde en la BD
        $tag->slug=Str::slug($request->name);

        //guardar informacion
        $tag->save();
        return redirect()->route('tags.index')->with('info', 'Agregados correctamente');
    }

    //function module para listar el modulo
    public function module($module)
    {
        $tags=Tag::where('module', $module)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.tags.index', compact('tags'));


    }

   
    public function show(Tag $tag)
    {
        //
    }

    
    public function edit($id)
    {
        //editar una categoria
        $tag=Tag::where('id', $id)->firstOrFail();
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');//a q categoria pertence el tag
        return view('admin.tags.edit', compact('tag', 'categories'));
    }

    
    public function update(Request $request, $id)
    {
        //se validan los datos
        $request->validate([
            'name'=> 'required|max:20',
        
        ]);

        //se actualiza con el $id
        $tag=Tag::findOrFail($id);
        $tag->name=e($request->name);
        $tag->category_id=e($request->category_id);
        $tag->slug=Str::slug($request->name);

         //se guarda y retorna a la vista
        $tag->save();
        return redirect()->route('tags.index')->with('info', 'Actualizado correctamente');

    }

    
    public function destroy($id)
    {
        //alimina la categoria
        $tag=Tag::findOrFail($id)->delete();
        //retorna a la vista anterior
        return back()->with('info', 'Eliminado con éxito');

    }
}
