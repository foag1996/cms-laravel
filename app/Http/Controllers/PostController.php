<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class PostController extends Controller
{
    //middleware para que pida la autenticacion
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() //lista todas las post registrados
    {
        $posts= Post::orderBy('id', 'DESC')->paginate(15);
        return view('admin.posts.index', compact('posts'));
    }


    public function create()
    {
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');//a q categoria pertence el post
        return view('admin.posts.create', compact('categories'));
    }


    public function store(Request $request)
    {
        //validacion
        $request->validate([
            'name'=> 'required|unique:posts|max:60',
            'user_id'=> 'required|integer',
            'category_id'=> 'required|integer',
            'abstract'=> 'required|max:500',
            'body'=> 'required',
            'status'=> 'required',
            'image'=>'image|dimensions:min_width=1200,max_width=1200,
            min_height=490, max_height=490|mimes:jpeg,jpg,png',//validacion de imagenes

        ]);

        //guardar la imagen validacion el el request if
        if ($request->hasFile('image')){
            $image= $request->file('image');
            $nombre= time().$image->getClientOriginalName();//se  captura la hora, se concatena con el nombre original del formulario
            $ruta=public_path().'/images'; //ruta donde se guardan las imagenes
            $image->move($ruta,$nombre);//se obtiene la imagen guardada en esta carpeta
            $urlimage['url']='/images/'.$nombre;//almacenar la imagen, el url y nombre. se concatena el url y el nombre

        }

        //slug automatico, la (e) es para que guarde en la BD
        $post=new Post;
        $post->user_id=e($request->user_id);
        $post->category_id=e($request->category_id);
        $post->name=e($request->name);
        $post->slug=Str::slug($request->name);
        $post->abstract=e($request->abstract);
        $post->body=e($request->body);
        $post->status=e($request->status);

        //guardar informacion
        $post->save();
        $post->image()->create($urlimage);//se vincule la imagen con el post creado
        return redirect()->route('posts.index')->with('info', 'Agregado correctamente');
    }

    //function module para listar el modulo
    public function module($module)
    {
        $posts=Post::where('module', $module)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.posts.index', compact('posts'));


    }


    public function show(Post $post)
    {

    }


    public function edit($id)
    {
        //editar una post
        $post=Post::where('id', $id)->firstOrFail();
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('admin.posts.edit', compact('post', 'categories'));
    }


    public function update(Request $request, $id)
    {
        //se validan los datos
        $request->validate([
            'name'=>'required|max:60',
            'user_id'=>'required|integer',
            'category_id'=>'required|integer',
            'abstract'=>'required|max:500',
            'body'=>'required',
            'status'=>'required',
            'image'=>'image|dimensions:min_width=1200, max_width=1200,min_height=490,
             max_height=490|mimes:jpeg,jpg,png',//validacion de imagenes

        ]);

        //guardar la imagen validacion el el request if

        if($request->hasFile('image')){
            $image=$request->file('image');
            $nombre = time().$image->getClientOriginalName();//se  captura la hora, se concatena con el nombre original del formulario
            $ruta = public_path().'/images'; //ruta donde se guardan las imagenes
            $image->move($ruta,$nombre);//se obtiene la imagen guardada en esta carpeta
            $urlimage['url']='/images/'.$nombre;//almacenar la imagen, el url y nombre. se concatena el url y el nombre

        }

        //se actualiza con el $id
        $post = Post::findOrFail($id);
        $post->user_id = e($request->user_id);
        $post->category_id = e($request->category_id);
        $post->name = e($request->name);
        $post->slug = Str::slug($request->name);
        $post->abstract = e($request->abstract);
        $post->body = e($request->body);
        $post->status = e($request->status);

        if ($request->hasFile('image')){
            $post->image()->delete();

         //se guarda y retorna a la vista
        }
        $post->save();
        if ($request->hasFile('image')){
            $post->image()->create($urlimage);//se vincule la imagen con el post creado
        }
        return redirect()->route('posts.index')->with('info', 'Actualizado correctamente');

    }


    public function destroy($id)
    {
        //alimina la categoria
        $post=Post::findOrFail($id)->delete();
        //retorna a la vista anterior
        return back()->with('info', 'Eliminado con éxito');

    }
}
