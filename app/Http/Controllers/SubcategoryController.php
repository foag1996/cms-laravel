<?php

namespace App\Http\Controllers;

use App\Subcategory;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class SubcategoryController extends Controller
{
    public function index() //lista todas las categorias registradas
    {
        $subcategories= Subcategory::orderBy('id', 'DESC')->paginate(15);
        return view('admin.subcategories.index', compact('subcategories'));
    }

   
    public function create()
    {
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');//a q categoria pertence la subcategoria
        return view('admin.subcategories.create', compact('categories'));
    }

    
    public function store(Request $request)
    {
        //validacion
        $request->validate([
            'name'=> 'required|unique:subcategories|max:20',
            
        ]);

        //slug automatico, la (e) es para que guarde en la BD
        $subcategory=new Subcategory;
        $subcategory->category_id=e($request->category_id);
        $subcategory->name=e($request->name);
        $subcategory->slug=Str::slug($request->name);

        //guardar informacion
        $subcategory->save();
        return redirect()->route('subcategories.index')->with('info', 'Subcategoría agregada');
    }

    //function module para listar el modulo
    public function module($module)
    {
        $subcategories=Subcategory::where('module', $module)->orderBy('id', 'DESC')->paginate(15);
        return view('admin.subcategories.index', compact('subcategories'));


    }

   
    public function show(Subcategory $subcategory)
    {
        //
    }

    
    public function edit($id)
    {
        //editar una categoria
        $subcategory=Subcategory::where('id', $id)->firstOrFail();
        $categories=Category::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('admin.subcategories.edit', compact('subcategory', 'categories'));
    }

    
    public function update(Request $request, $id)
    {
        //se validan los datos
        $request->validate([
            'name'=> 'required|max:20',
            
        ]);

        //se actualiza con el $id
        $subcategory=Subcategory::findOrFail($id);
        $subcategory->category_id=e($request->category_id);
        $subcategory->name=e($request->name);
        $subcategory->slug=Str::slug($request->name);

         //se guarda y retorna a la vista
        $subcategory->save();
        return redirect()->route('subcategories.index')->with('info', 'Actualizado correctamente');

    }

    
    public function destroy($id)
    {
        //alimina la categoria
        $subcategory=Subcategory::findOrFail($id)->delete();
        //retorna a la vista anterior
        return back()->with('info', 'Eliminado con éxito');

    }
}
