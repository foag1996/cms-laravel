<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Ruta tipo resource que trae todo el CRUD, muestra todo
Route::resource('admin/categories', 'CategoryController')->names('categories');
//ruta de module
Route::get('admin/category/{module}', 'CategoryController@module')->name('categories.module');
//ruta subcategorias
Route::resource('admin/subcategories', 'SubcategoryController')->names('subcategories');
//ruta tags
Route::resource('admin/tags', 'TagController')->names('tags');
//ruta posts
Route::resource('admin/posts', 'PostController')->names('posts');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
